<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Update Customer Details (Blacklist) | adminTele" />
    <title>Update Customer Details (Blacklist) | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'teleSidebar.php'; ?>
<div class="next-to-sidebar">
  <!-- <form method="POST" action="utilities/updateCustomerDetailsFunction.php" enctype="multipart/form-data"> -->
  <form method="POST" action="utilities/updateCustomerDetailsBlackFunction.php" enctype="multipart/form-data">

	<h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            <!-- <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back"> -->
        	Customer ID : <?php echo $_POST['customer_id']; ?>
        </a>
    </h1>

   
        <?php
            if(isset($_POST['customer_id']))
            {
                $conn = connDB();
                $customerDetails = getCustomerDetails($conn,"WHERE id = ? ", array("id") ,array($_POST['customer_id']),"i");
            ?>

        <div class="input50-div">
			<p class="input-title-p">Name</p>
            <p class="clean tele-input clean-bg"><?php echo $customerDetails[0]->getName();?></p>       
        </div> 
        <div class="input50-div second-input50">
			<p class="input-title-p">Phone</p>
            <p class="clean tele-input clean-bg"><?php echo $customerDetails[0]->getPhone();?></p>       
        </div> 
        <div class="clear"></div>
        <div class="input50-div">
			<p class="input-title-p">Email</p>
            <p class="clean tele-input clean-bg"><?php echo $customerDetails[0]->getEmail();?></p>       
        </div> 
        <div class="input50-div second-input50">
			<p class="input-title-p">Status</p>
                        <select class="clean tele-input" id="update_status" value="<?php echo $customerDetails[0]->getStatus();?>" name="update_status">
                            <?php
                                if($customerDetails[0]->getStatus() == '')
                                {
                                    ?>
                                    <option value="Bad"  name='Bad'>Bad</option>
                                    <option value="Good"  name='Good'>Good</option>
                                    <option selected value=""  name=''></option>
                                    <?php
                                }
                                else if($customerDetails[0]->getStatus() == 'Good')
                                {
                                    ?>
                                    <option value="Bad"  name='Bad'>Bad</option>
                                    <option selected value="Good"  name='Good'>Good</option>
                                    <?php
                                }
                                else if($customerDetails[0]->getStatus() == 'Bad')
                                {
                                    ?>
                                    <option selected value="Bad"  name='Bad'>Bad</option>
                                    <option value="Good"  name='Good'>Good</option>
                                    <?php
                                }
                            ?>
                        </select>
                    </td>
                </tr>
			</div>
        <div class="clear"></div> 
        <div class="width100">
        	<p class="input-title-p">Remark</p>

				<input class="clean tele-input" type="text" value="<?php echo $customerDetails[0]->getRemark();?>" placeholder="remark" id="update_remark" name="update_remark">

		</div>
                <input type="hidden" id="id" name="id" value="<?php echo $customerDetails[0]->getId() ?>">
                <input type="hidden" id="no_of_call" name="no_of_call" value="<?php echo $customerDetails[0]->getNoOfCall() ?>">
                <input type="hidden" id="customer_name" name="customer_name" value="<?php echo $customerDetails[0]->getName() ?>">

                <input type="hidden" id="tele_username" name="tele_username" value="<?php echo $userDetails->getUsername();?>">
                <input type="hidden" id="tele_uid" name="tele_uid" value="<?php echo $userDetails->getUid();?>">
            
            <?php
            }
        ?>


    <div class="clear"></div>

    <!-- <button class="clean red-btn margin-top30 fix300-btn" name="refereeButton">Submit</button> -->
    <button class="clean red-btn margin-top30 fix300-btn" type="submit" id = "editSubmit" name = "editSubmit" ><b>CONFIRM</b></a></button>

    <!-- <button class="clean red-btn margin-top30 fix300-btn" type="submit" id ="editSubmit" name ="editSubmit" ><b>Submit</b></a></button> -->

</div>
</form>
</div>
<style>
.dashboard-li{
	color:#bf1b37;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>
<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>
