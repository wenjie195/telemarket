<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/CustomerDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    $id = $_POST["id"];
    $update_status = $_POST["update_status"];
    $update_remark = $_POST["update_remark"];

    $no_of_call = $_POST["no_of_call"];
    $update_no_of_call = $no_of_call + 1;

    $tele_username = $_POST["tele_username"];
    $tele_uid = $_POST["tele_uid"];
    $customer_name = $_POST["customer_name"];
}

if(isset($_POST['editSubmit']))
{
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    // //echo "save to database";
    if($update_status)
    {
        array_push($tableName,"status");
        array_push($tableValue,$update_status);
        $stringType .=  "s";
    }
    if($update_remark)
    {
        array_push($tableName,"remark");
        array_push($tableValue,$update_remark);
        $stringType .=  "s";
    }
    if($update_no_of_call)
    {
        array_push($tableName,"no_of_call");
        array_push($tableValue,$update_no_of_call);
        $stringType .=  "s";
    }

    array_push($tableValue,$id);
    $stringType .=  "s";
    $updateCustomerDetails = updateDynamicData($conn,"customerdetails"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($updateCustomerDetails)
    {
        // $_SESSION['messageType'] = 1;
        // header('Location: ../teleDashboard.php');

        $uid = $tele_uid;
        $teleName = $tele_username;
        $customerName = $customer_name;
        $updateStatus = $update_status;
        $updateRemark = $update_remark;
        $noOfuUpdate = $update_no_of_call;

        // if (timeTeleUpdate($conn,$uid,$teleName,$customerName,$updateStatus,$updateRemark))
        if (timeTeleUpdate($conn,$uid,$teleName,$customerName,$updateStatus,$updateRemark,$noOfuUpdate))
        {
            echo "<script>alert('Data Updated !');window.location='../teleDashboard.php'</script>";   
        }
        else
        {}// echo "fail";

        // echo "<script>alert('Data Updated !');window.location='../teleDashboard.php'</script>";     
    }
    else
    {
        echo "<script>alert('Fail to Update Data !');window.location='../teleDashboard.php'</script>";     
        // header('Location: ../teleDashboard.php');
    }
}
else
{
  //  echo "dunno";

}

// function timeTeleUpdate($conn,$uid,$teleName,$customerName,$updateStatus,$updateRemark)
// {
//      if(insertDynamicData($conn,"time_teleupdate",array("uid","tele_name","customer_name","update_status","update_remark"),
//      array($uid,$teleName,$customerName,$updateStatus,$updateRemark),"sssss") === null)
//      {
//           return false;
//      }
//      else
//      {}
//      return true;
// }
function timeTeleUpdate($conn,$uid,$teleName,$customerName,$updateStatus,$updateRemark,$noOfuUpdate)
{
     if(insertDynamicData($conn,"time_teleupdate",array("uid","tele_name","customer_name","update_status","update_remark","no_of_update"),
     array($uid,$teleName,$customerName,$updateStatus,$updateRemark,$noOfuUpdate),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}
?>