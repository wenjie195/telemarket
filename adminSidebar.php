<div class="side-bar red-bg">
	<img src="img/logo.png" class="logo-img" alt="logo" title="logo">
	<ul class="sidebar-ul">
    	<a href="adminDashboard.php">
        	<li class="sidebar-li dashboard-li hover1">
            	<img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                 <p>Dashboard</p>
            </li>
        </a>
    	<a href="addReferee.php">
        	<li class="sidebar-li account-li hover1">
            	<img src="img/create-account1.png" class="hover1a" alt="Account Creation" title="Account Creation">
                <img src="img/create-account2.png" class="hover1b" alt="Account Creation" title="Account Creation">
                 <p>Account Creation</p>
            </li>
        </a>
        <a href="viewTeleList.php">
        	<li class="sidebar-li telemarketer-li hover1">
            	<img src="img/telemarketer1.png" class="hover1a" alt="Telemarketer" title="Telemarketer">
                <img src="img/telemarketer2.png" class="hover1b" alt="Telemarketer" title="Telemarketer">
                 <p>Telemarketer</p>
            </li>
        </a>
        <a href="checkLog.php">
        	<li class="sidebar-li customer-li hover1">
            	<img src="img/customer1.png" class="hover1a" alt="Customer" title="Customer">
                <img src="img/customer2.png" class="hover1b" alt="Customer" title="Customer">            
            	 <p>Customer</p>
            </li>
        </a>
        <a href="uploadExcel.php">
        	<li class="sidebar-li import-li hover1">
            	<img src="img/import-data1.png" class="hover1a" alt="Import Data" title="Import Data">
                <img src="img/import-data2.png" class="hover1b" alt="Import Data" title="Import Data">            
             	 <p>Import Data</p>
            </li>
        </a>
        <a href="logout.php">
        	<li class="sidebar-li hover1">
            	<img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
            	 <p>Logout</p>
            </li>
       </a>
    </ul>
</div>
<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <img src="img/logo.png" class="mobile-logo" alt="Logo" title="Logo">
        <div id="dl-menu" class="dl-menuwrapper">
            <button class="dl-trigger">Open Menu</button>
            <ul class="dl-menu">
                <a href="adminDashboard.php">
                    <li class="sidebar-li dashboard-li hover1">
                        <img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                        <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                         <p>Dashboard</p>
                    </li>
                </a>
                <a href="addReferee.php">
                    <li class="sidebar-li account-li hover1">
                        <img src="img/create-account1.png" class="hover1a" alt="Account Creation" title="Account Creation">
                        <img src="img/create-account2.png" class="hover1b" alt="Account Creation" title="Account Creation">
                         <p>Account Creation</p>
                    </li>
                </a>
                <a href="viewTeleList.php">
                    <li class="sidebar-li telemarketer-li hover1">
                        <img src="img/telemarketer1.png" class="hover1a" alt="Telemarketer" title="Telemarketer">
                        <img src="img/telemarketer2.png" class="hover1b" alt="Telemarketer" title="Telemarketer">
                         <p>Telemarketer</p>
                    </li>
                </a>
                <a href="checkLog.php">
                    <li class="sidebar-li customer-li hover1">
                        <img src="img/customer1.png" class="hover1a" alt="Customer" title="Customer">
                        <img src="img/customer2.png" class="hover1b" alt="Customer" title="Customer">            
                         <p>Customer</p>
                    </li>
                </a>
                <a href="uploadExcel.php">
                    <li class="sidebar-li import-li hover1">
                        <img src="img/import-data1.png" class="hover1a" alt="Import Data" title="Import Data">
                        <img src="img/import-data2.png" class="hover1b" alt="Import Data" title="Import Data">            
                         <p>Import Data</p>
                    </li>
                </a>
                <a href="logout.php">
                    <li class="sidebar-li hover1">
                        <img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                        <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
                         <p>Logout</p>
                    </li>
               </a>
            </ul>
    </div>
</header>