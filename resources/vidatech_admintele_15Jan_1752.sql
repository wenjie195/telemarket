-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2020 at 10:52 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_admintele`
--

-- --------------------------------------------------------

--
-- Table structure for table `customerdetails`
--

CREATE TABLE `customerdetails` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `last_updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `no_of_call` varchar(255) DEFAULT '0',
  `update_status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customerdetails`
--

INSERT INTO `customerdetails` (`id`, `name`, `phone`, `email`, `status`, `remark`, `last_updated`, `no_of_call`, `update_status`, `date_created`, `date_updated`) VALUES
(1, 'Marc-Andre ter Stegen', '1', 'terstegen@barca.cc', '5th', '5thee', '2020-01-15 09:50:57', '5', 'BAD', '2020-01-15 09:47:21', '2020-01-15 09:50:57'),
(2, 'Nelson Semedo', '2', 'semedo@barca.cc', '', '', '2020-01-15 09:47:21', '0', NULL, '2020-01-15 09:47:21', '2020-01-15 09:47:21'),
(3, 'Gerard Pique', '3', 'pique@barca.cc', '', '', '2020-01-15 09:47:21', '0', NULL, '2020-01-15 09:47:21', '2020-01-15 09:47:21'),
(4, 'Ivan Rakitic', '4', 'rakitic@barca.cc', '', '', '2020-01-15 09:47:21', '0', NULL, '2020-01-15 09:47:21', '2020-01-15 09:47:21'),
(5, 'Sergio Busquest', '5', 'sergio@barca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22'),
(6, 'Jean-Clair Todibo', '6', 'todibo@barca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22'),
(7, 'Arthur', '8', 'arthur@barca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22'),
(8, 'Luis Suarez', '9', 'suqrez@baarca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22'),
(9, 'Lionel Messi', '10', 'messi@barca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22'),
(10, 'Ousmane Dembele', '11', 'dembele@barca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22');

-- --------------------------------------------------------

--
-- Table structure for table `excel`
--

CREATE TABLE `excel` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `last_updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `no_of_call` varchar(255) DEFAULT '0',
  `update_status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `excel`
--

INSERT INTO `excel` (`id`, `name`, `phone`, `email`, `status`, `remark`, `last_updated`, `no_of_call`, `update_status`, `date_created`, `date_updated`) VALUES
(1, 'Marc-Andre ter Stegen', '1', 'terstegen@barca.cc', '', '', '2020-01-15 09:47:21', '0', NULL, '2020-01-15 09:47:21', '2020-01-15 09:47:21'),
(2, 'Nelson Semedo', '2', 'semedo@barca.cc', '', '', '2020-01-15 09:47:21', '0', NULL, '2020-01-15 09:47:21', '2020-01-15 09:47:21'),
(3, 'Gerard Pique', '3', 'pique@barca.cc', '', '', '2020-01-15 09:47:21', '0', NULL, '2020-01-15 09:47:21', '2020-01-15 09:47:21'),
(4, 'Ivan Rakitic', '4', 'rakitic@barca.cc', '', '', '2020-01-15 09:47:21', '0', NULL, '2020-01-15 09:47:21', '2020-01-15 09:47:21'),
(5, 'Sergio Busquest', '5', 'sergio@barca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22'),
(6, 'Jean-Clair Todibo', '6', 'todibo@barca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22'),
(7, 'Arthur', '8', 'arthur@barca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22'),
(8, 'Luis Suarez', '9', 'suqrez@baarca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22'),
(9, 'Lionel Messi', '10', 'messi@barca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22'),
(10, 'Ousmane Dembele', '11', 'dembele@barca.cc', '', '', '2020-01-15 09:47:22', '0', NULL, '2020-01-15 09:47:22', '2020-01-15 09:47:22');

-- --------------------------------------------------------

--
-- Table structure for table `time_teleupdate`
--

CREATE TABLE `time_teleupdate` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `tele_name` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `update_status` varchar(255) DEFAULT NULL,
  `update_remark` varchar(255) DEFAULT NULL,
  `no_of_update` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_teleupdate`
--

INSERT INTO `time_teleupdate` (`id`, `uid`, `tele_name`, `customer_name`, `update_status`, `update_remark`, `no_of_update`, `date_created`, `date_updated`) VALUES
(1, '4f721b69cabc73d748521d810eacf306', 'tele1', 'Marc-Andre ter Stegen', '1st', '1staa', '1', '2020-01-15 09:47:42', '2020-01-15 09:47:42'),
(2, '4f721b69cabc73d748521d810eacf306', 'tele1', 'Marc-Andre ter Stegen', '2nd', '2ndbb', '2', '2020-01-15 09:47:53', '2020-01-15 09:47:53'),
(3, '4f721b69cabc73d748521d810eacf306', 'tele1', 'Marc-Andre ter Stegen', '3rd', '3rdcc', '3', '2020-01-15 09:48:01', '2020-01-15 09:48:01'),
(4, 'f877f0c397a187a2aba6e301d76e1e0f', 'aa', 'Marc-Andre ter Stegen', 'BAD', '4thdd', '4', '2020-01-15 09:49:18', '2020-01-15 09:49:18'),
(5, 'f877f0c397a187a2aba6e301d76e1e0f', 'aa', 'Marc-Andre ter Stegen', '5th', '5thee', '5', '2020-01-15 09:50:57', '2020-01-15 09:50:57');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `nationality`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9ca817799c575c1afd37cccf6a22aa7f', 'admin', 'admin@gmail.com', 'fe8192029d7b39d66fe035ea96623f6ab6f06c961985d12f210b353764b1e90a', 'e74df95cbd449eb12c45c8685694897a6869037f', '12351236', 'admin', NULL, 1, 0, '2020-01-14 06:36:33', '2020-01-14 08:28:41'),
(6, '4f721b69cabc73d748521d810eacf306', 'tele1', 'tele1@gg.cc', '2e496470a8d3280aaaf5a7f0be204a75531d6280c77371f8d7b99f0a349b5643', 'd16d6fa72b0f71072146d384c88e99bc1698e2ba', '00-11-22', 'tele1 tele1', NULL, 1, 1, '2020-01-15 09:46:46', '2020-01-15 09:46:46'),
(7, 'f877f0c397a187a2aba6e301d76e1e0f', 'aa', 'aa@gg.cc', 'c2db4d928f715a9ec2a50caad33d65483594d6cbc957890b98c10647ae90ef16', '4f9747205249516ed2095271d20e8093901398e9', '0-11223', 'aassdd', NULL, 1, 1, '2020-01-15 09:47:04', '2020-01-15 09:47:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customerdetails`
--
ALTER TABLE `customerdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excel`
--
ALTER TABLE `excel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_teleupdate`
--
ALTER TABLE `time_teleupdate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customerdetails`
--
ALTER TABLE `customerdetails`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `excel`
--
ALTER TABLE `excel`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `time_teleupdate`
--
ALTER TABLE `time_teleupdate`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
