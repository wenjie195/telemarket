-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2020 at 03:46 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_admintele`
--

-- --------------------------------------------------------

--
-- Table structure for table `customerdetails`
--

CREATE TABLE `customerdetails` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `last_updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `no_of_call` varchar(255) DEFAULT '0',
  `update_status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customerdetails`
--

INSERT INTO `customerdetails` (`id`, `name`, `phone`, `email`, `status`, `remark`, `last_updated`, `no_of_call`, `update_status`, `date_created`, `date_updated`) VALUES
(1, 'Marc-Andre ter Stegen', '1', 'terstegen@barca.cc', 'Bad', 'transfer to black list', '2020-01-16 01:27:57', '3', NULL, '2020-01-16 01:23:57', '2020-01-16 01:27:57'),
(2, 'Nelson Semedo', '2', 'semedo@barca.cc', 'Good', 'case submit', '2020-01-16 01:30:23', '2', NULL, '2020-01-16 01:23:57', '2020-01-16 01:30:23'),
(3, 'Gerard Pique', '3', 'pique@barca.cc', '', 'unreachable', '2020-01-16 01:29:49', '1', NULL, '2020-01-16 01:23:57', '2020-01-16 01:29:49'),
(4, 'Ivan Rakitic', '4', 'rakitic@barca.cc', 'Good', '345', '2020-01-16 02:45:02', '1', NULL, '2020-01-16 01:23:57', '2020-01-16 02:45:02'),
(5, 'Sergio Busquest', '5', 'sergio@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(6, 'Jean-Clair Todibo', '6', 'todibo@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(7, 'Arthur', '8', 'arthur@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(8, 'Luis Suarez', '9', 'suqrez@baarca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(9, 'Lionel Messi', '10', 'messi@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(10, 'Ousmane Dembele', '11', 'dembele@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57');

-- --------------------------------------------------------

--
-- Table structure for table `excel`
--

CREATE TABLE `excel` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `last_updated` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `no_of_call` varchar(255) DEFAULT '0',
  `update_status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `excel`
--

INSERT INTO `excel` (`id`, `name`, `phone`, `email`, `status`, `remark`, `last_updated`, `no_of_call`, `update_status`, `date_created`, `date_updated`) VALUES
(1, 'Marc-Andre ter Stegen', '1', 'terstegen@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(2, 'Nelson Semedo', '2', 'semedo@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(3, 'Gerard Pique', '3', 'pique@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(4, 'Ivan Rakitic', '4', 'rakitic@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(5, 'Sergio Busquest', '5', 'sergio@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(6, 'Jean-Clair Todibo', '6', 'todibo@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(7, 'Arthur', '8', 'arthur@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(8, 'Luis Suarez', '9', 'suqrez@baarca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(9, 'Lionel Messi', '10', 'messi@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57'),
(10, 'Ousmane Dembele', '11', 'dembele@barca.cc', '', '', '2020-01-16 01:23:57', '0', NULL, '2020-01-16 01:23:57', '2020-01-16 01:23:57');

-- --------------------------------------------------------

--
-- Table structure for table `time_teleupdate`
--

CREATE TABLE `time_teleupdate` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `tele_name` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `update_status` varchar(255) DEFAULT NULL,
  `update_remark` varchar(255) DEFAULT NULL,
  `no_of_update` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_teleupdate`
--

INSERT INTO `time_teleupdate` (`id`, `uid`, `tele_name`, `customer_name`, `update_status`, `update_remark`, `no_of_update`, `date_created`, `date_updated`) VALUES
(1, '9cf05ae7c8578b74a0a3a54fdf7db3c6', 'tele1', 'Marc-Andre ter Stegen', '', '1st call no reply', '1', '2020-01-16 01:27:26', '2020-01-16 01:27:26'),
(2, '9cf05ae7c8578b74a0a3a54fdf7db3c6', 'tele1', 'Marc-Andre ter Stegen', '', '2nd call no reply too', '2', '2020-01-16 01:27:40', '2020-01-16 01:27:40'),
(3, '9cf05ae7c8578b74a0a3a54fdf7db3c6', 'tele1', 'Marc-Andre ter Stegen', 'Bad', 'transfer to black list', '3', '2020-01-16 01:27:57', '2020-01-16 01:27:57'),
(4, '9cf05ae7c8578b74a0a3a54fdf7db3c6', 'tele1', 'Nelson Semedo', 'Good', 'answer, success', '1', '2020-01-16 01:28:23', '2020-01-16 01:28:23'),
(5, 'add09c153ff68c985d44361dfb7c7a56', 'aa', 'Gerard Pique', '', 'unreachable', '1', '2020-01-16 01:29:49', '2020-01-16 01:29:49'),
(6, 'add09c153ff68c985d44361dfb7c7a56', 'aa', 'Nelson Semedo', 'Good', 'case submit', '2', '2020-01-16 01:30:23', '2020-01-16 01:30:23'),
(7, '7417e27505f64b9bf87e37c34f236f38', 'WenJie Tay', 'Ivan Rakitic', 'Good', '345', '1', '2020-01-16 02:45:02', '2020-01-16 02:45:02');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `address`, `nationality`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9ca817799c575c1afd37cccf6a22aa7f', 'admin', 'admin@gmail.com', 'fe8192029d7b39d66fe035ea96623f6ab6f06c961985d12f210b353764b1e90a', 'e74df95cbd449eb12c45c8685694897a6869037f', '12351236', 'admin', NULL, NULL, 1, 0, '2020-01-14 06:36:33', '2020-01-14 08:28:41'),
(2, '9cf05ae7c8578b74a0a3a54fdf7db3c6', 'tele1', 'tele1@gg.cc', 'b811c3093eeb9f7ab523ba918c9ddab53ba99746aeecc05f663c17ba79e40552', '934b9462204daa1890d23bc11f03941762a814dc', '+60-123', 'tele1 tele1', NULL, NULL, 1, 1, '2020-01-16 01:23:35', '2020-01-16 01:23:35'),
(3, 'add09c153ff68c985d44361dfb7c7a56', 'aa', 'aa@gg.cc', 'dcba42d4bdecdc6e792b3afa6ec300e77134a574ce9fcb92562ea55ab372e8c4', 'bcfef90c9796645cc7aa077dcbbd2d466d796437', '+123-123', 'AAssdd', NULL, NULL, 1, 1, '2020-01-16 01:29:17', '2020-01-16 01:29:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customerdetails`
--
ALTER TABLE `customerdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excel`
--
ALTER TABLE `excel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_teleupdate`
--
ALTER TABLE `time_teleupdate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customerdetails`
--
ALTER TABLE `customerdetails`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `excel`
--
ALTER TABLE `excel`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `time_teleupdate`
--
ALTER TABLE `time_teleupdate`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
