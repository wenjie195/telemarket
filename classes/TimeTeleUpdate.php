<?php
//class User {
class TimeTeleUpdate {
    /* Member variables */
    // var $id, $name, $phone, $email, $status, $remark, $lastUpdated ,$noOfCall, $updateStatus, $dateCreated, $dateUpdated;
    var $id, $uid, $teleName, $customerName, $updateStatus, $updateRemark, $noOfUpdate, $dateCreated, $dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
            
    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getTeleName()
    {
        return $this->teleName;
    }

    /**
     * @param mixed $teleName
     */
    public function setTeleName($teleName)
    {
        $this->teleName = $teleName;
    }

    /**
     * @return mixed
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @param mixed $customerName
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * @return mixed
     */
    public function getUpdateStatus()
    {
        return $this->updateStatus;
    }

    /**
     * @param mixed $updateStatus
     */
    public function setUpdateStatus($updateStatus)
    {
        $this->updateStatus = $updateStatus;
    }

    /**
     * @return mixed
     */
    public function getUpdateRemark()
    {
        return $this->updateRemark;
    }

    /**
     * @param mixed $updateRemark
     */
    public function setUpdateRemark($updateRemark)
    {
        $this->updateRemark = $updateRemark;
    }

    /**
     * @return mixed
     */
    public function getNoOfUpdate()
    {
        return $this->noOfUpdate;
    }

    /**
     * @param mixed $noOfUpdate
     */
    public function setNoOfUpdate($noOfUpdate)
    {
        $this->noOfUpdate = $noOfUpdate;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getTimeTeleUpdate($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","tele_name","customer_name","update_status","update_remark","no_of_update","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"time_teleupdate");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $name, $phone, $email, $status, $remark, $lastUpdated ,$noOfCall, $updateStatus, $dateCreated, $dateUpdated);

        $stmt->bind_result($id, $uid, $teleName, $customerName, $updateStatus, $updateRemark, $noOfUpdate, $dateCreated, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new TimeTeleUpdate;
            $class->setId($id);
            $class->setUid($uid);
            $class->setTeleName($teleName);
            $class->setCustomerName($customerName);
            $class->setUpdateStatus($updateStatus);
            $class->setUpdateRemark($updateRemark);
            $class->setNoOfUpdate($noOfUpdate);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
