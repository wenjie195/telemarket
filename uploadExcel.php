<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

include 'selectFilecss.php';
include 'js.php';

// if ($_SESSION['lang'] == 1)
// {
//   include 'lang/en.php';
// }
// elseif($_SESSION['lang'] == 2)
// {
//   include 'lang/ch.php';
// }

// $conn = mysqli_connect("localhost","root","","phpsamples");
// $conn = mysqli_connect("localhost","ppayshop_sample","Along_12345","ppayshop_phpsamples");
// $conn = mysqli_connect("localhost","ppayshop_admin","Along_12345","ppayshop_ppay_database");
require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    $targetPath = 'uploads/'.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {
        // $userID = "";
        // if(isset($Row[0])) 
        // {
        //   $userID = mysqli_real_escape_string($conn,$Row[0]);
        // }
        // $userName = "";
        // if(isset($Row[0])) 
        // {
        //   $userName = mysqli_real_escape_string($conn,$Row[1]);
        // }
        // if (!empty($userName) || !empty($userID))


        $name = "";
        if(isset($Row[0])) 
        {
          $name = mysqli_real_escape_string($conn,$Row[0]);
        }
        $phone = "";
        if(isset($Row[0])) 
        {
          $phone = mysqli_real_escape_string($conn,$Row[1]);
        }
        $email = "";
        if(isset($Row[0])) 
        {
          $email = mysqli_real_escape_string($conn,$Row[2]);
        }
        if (!empty($name) || !empty($phone)|| !empty($email))
        {
          // $query = "INSERT INTO excel (user_id,username) VALUES ('".$userID."','".$userName."') ";
          $query = "INSERT INTO excel (name,phone,email) VALUES ('".$name."','".$phone."','".$email."') ";
          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
            // $type = "success";
            // $message = "Excel Data Imported into the Database";
            // $_SESSION['messageType'] = 3;
            // header("location: addMember.php?type=3");
            // echo"Excel Data Imported into the Database";  

              $query = "INSERT INTO customerdetails (name,phone,email) VALUES ('".$name."','".$phone."','".$email."') ";
              $result = mysqli_query($conn, $query);
              if (! empty($result))
              {
                // echo "<script>alert('Excel Uploaded !');window.location='../adTele/uploadExcel.php'</script>";   
                echo "<script>alert('Excel Uploaded !');window.location='../telemarket/uploadExcel.php'</script>";         
              }
              else 
              {
                // $type = "error";
                // $message = "Problem in Importing Excel Data";
                // echo "<script>alert('Problem in Importing Excel Data !');window.location='../adTele/uploadExcel.php'</script>";
                echo "<script>alert('Problem in Importing Excel Data !');window.location='../telemarket/uploadExcel.php'</script>";
              }      
          }
          else 
          {
            // $type = "error";
            // $message = "Problem in Importing Excel Data";
            // echo "<script>alert('Problem in Importing Excel Data !');window.location='../adTele/uploadExcel.php'</script>";
            echo "<script>alert('Problem in Importing Excel Data !');window.location='../telemarket/uploadExcel.php'</script>";
          }
        }
      }
    }
  }
  else
  {
  // $type = "error";
  // $message = "Invalid File Type. Upload Excel File.";
  // echo "<script>alert('Invalid File Type. Upload Excel File.');window.location='../adTele/uploadExcel.php'</script>";
  echo "<script>alert('Invalid File Type. Upload Excel File.');window.location='../telemarket/uploadExcel.php'</script>";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Import Data | adminTele" />
    <title>Import Data | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>


  <body class="body">
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar"> 
  
    <h1 class="h1-title">Import Excel File</h1>

      <div class="outer-container">
        <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
          <div>
            <!-- <label><?php echo $choose ?></label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"><div class="clear"></div>
            <button type="submit" id="submit" name="import" class="btn-hover color-1"><?php echo $import ?></button>
            <a href="dashboard.php">
              <button type="button" class="btn-hover color-8"><?php echo $cancel; ?></button>
            </a> -->

            <label>Select File</label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"><div class="clear"></div>
            <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button>

          </div>
        </form>
      </div>

      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>

</div>
<style>
.import-li{
	color:#bf1b37;
	background-color:white;}
.import-li .hover1a{
	display:none;}
.import-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>
<?php
if (isset($_GET['type'])) 
{
  $messageType = null;
  if ($_SESSION['messageType'] == 1) 
  {
    ?>
      <script>
        Notiflix.Notify.Success('Login Success');
      </script>
    <?php
  }
  if ($_SESSION['messageType'] == 3) 
  {
    ?>
      <script>
        Notiflix.Notify.Success('Update Success');
      </script> 
    <?php
  }
}
?>


  </body>

</html>
