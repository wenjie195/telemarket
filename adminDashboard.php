<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Admin Dashboard | adminTele" />
    <title>Admin Dashboard | adminTele</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">
    <h1 class="h1-title">Admin Dashboard</h1>

    <div class="clear"></div>
    <div class="width100 overflow">
        <a href="addReferee.php">
            <div class="four-div white-to-pink-div hover1">
                <img src="img/create-account-big.png" class="big-icon hover1a" alt="Create Account" title="Create Account">
                <img src="img/create-account-big2.png" class="big-icon hover1b" alt="Create Account" title="Create Account">
                <p>Account Creation</p>
            </div>
        </a>
        <a href="viewTeleList.php">
            <div class="four-div left-mid-four white-to-pink-div hover1">
                <img src="img/telemarketer-big.png" class="big-icon hover1a"  alt="Telemarketer" title="Telemarketer">
                <img src="img/telemarketer-big2.png" class="big-icon hover1b" alt="Telemarketer" title="Telemarketer">
                <p>Telemarketer</p>
            </div>
        </a>
        <div class="tempo-2-clear"></div>
        <a href="checkLog.php">
            <div class="four-div right-mid-four white-to-pink-div hover1">
                <img src="img/customer-big.png" class="big-icon hover1a"  alt="Customer" title="Customer">
                <img src="img/customer-big2.png" class="big-icon hover1b" alt="Customer" title="Customer">
                <p>Customer</p>
            </div>
        </a>    
        <a href="uploadExcel.php">
            <div class="four-div white-to-pink-div hover1">
                <img src="img/import-data-big.png" class="big-icon hover1a"  alt="Import Data" title="Import Data">
                <img src="img/import-data-big2.png" class="big-icon hover1b"  alt="Import Data" title="Import Data">
                <p>Import Data</p>
            </div>
        </a>     
	</div>
    <div class="clear"></div>

        
</div>
<style>
.dashboard-li{
	color:#bf1b37;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style>
<?php include 'js.php'; ?>
</body>
</html>